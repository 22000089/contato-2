//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit
import Alamofire
import  Kingfisher

struct Harry: Decodable {
    let actor: String
    let name: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaHarry:[Harry] = []
    var listaDeContatos:[Harry] = []
    
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey = "ListaDeContato"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaHarry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaHarry[indexPath.row]
        
        celula.nome.text = contato.name
        celula.email.text = contato.actor
        celula.imagemHarry.kf.setImage(with: URL(string: contato.image))
        
        return celula
    }
    
    func getNovoHarry() {
            AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Harry].self) { response in
                if let cachorro = response.value {
                    self.listaHarry = cachorro
                    print(cachorro)
                }
                self.tableview.reloadData()
            }
        }
    

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        getNovoHarry()
        
        

        
        
        
    }
    
    
}

